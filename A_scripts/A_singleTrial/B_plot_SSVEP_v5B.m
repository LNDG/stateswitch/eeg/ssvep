%% load all SSVEP v5 data

% v5 includes frequencies in 1 Hz steps, not just 30 +-2 Hz

clear all; clc;

% N = 47; N = 53 OAs
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

OriginalSSVEP = [];
for indID = 1:numel(IDs)
    tmp = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/B_data/E_singleTrialFFT/',IDs{indID},'_SSVEP_FFT_singleTrial_byCond_v5.mat']);
    for indCond = 1:4
        OriginalSSVEP(indID,indCond,:,:,:) = tmp.MTMdata{indCond}.powspctrm_avg;
    end
    time = tmp.MTMdata{indCond}.time;
    freq = tmp.MTMdata{indCond}.freq;
end

% spectral correction

% NormalizedSSVEP = [];
% for indFreq = 3:30-2
%    NormalizedSSVEP(:,:,:,:,indFreq) = 10*log10(OriginalSSVEP(:,:,:,:,indFreq)./nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1, indFreq+1]),5));
% end

for indFreq = 2:30-1
   NormalizedSSVEP(:,:,:,:,indFreq) = (OriginalSSVEP(:,:,:,:,indFreq)-nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5))./nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5);
end

blTimeIdx = find(tmp.MTMdata{indCond}.time > 2.3 & tmp.MTMdata{indCond}.time <2.9);

originalBaseline = repmat(nanmean(nanmean(OriginalSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
normalizedBaseline = repmat(nanmean(nanmean(NormalizedSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
%normalizedBaseline_st = repmat(nanmean(nanmean(NormalizedSSVEP_ST(:,:,:,blTimeIdx),4),2),1,4,1,66);

OriginalSSVEP_bl = (OriginalSSVEP-originalBaseline)./originalBaseline;
NormalizedSSVEP_bl = (NormalizedSSVEP-normalizedBaseline);
%NormalizedSSVEP_bl_st = (NormalizedSSVEP_ST-normalizedBaseline_st);

%% save SSVEP as output

SSVEPmag.norm.data = squeeze(nanmean(nanmean(NormalizedSSVEP_bl(:,1:4,58:60,time>3& time<6, 15),3),4));
SSVEPmag.orig.data = squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,58:60,time>3& time<6, 15),3),4));
SSVEPmag.IDs = IDs;

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/B_data/E_singleTrialFFT/Z_SSVEPmag.mat', 'SSVEPmag')

%%


h = figure('units','normalized','position',[.1 .1 .6 .5]);
subplot(2,2,1); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('Original, non-normalized')
subplot(2,2,2); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('Original, temp. normalized')
subplot(2,2,3); imagesc(time,freq,squeeze(nanmean(nanmean(NormalizedSSVEP(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('spectrally normalized')
subplot(2,2,4); imagesc(time,freq,squeeze(nanmean(nanmean(NormalizedSSVEP_bl(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('spectrally, temp. normalized')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_TFR_OriginalNormalized';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot original and spectrally normalized spectra

h = figure('units','normalized','position',[.1 .1 .4 .2]);
subplot(1,2,1); hold on;
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,1,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,2,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,3,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,4,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    xlabel('Frequency (Hz)'); ylabel('Power (log10)');
    title('Non-normalized estimate');
    legend({'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}); legend('boxoff')
subplot(1,2,2); hold on;
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,1,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,2,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,3,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,4,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    xlabel('Frequency (Hz)'); ylabel('Power (norm.)');
    title('Spectrally-normalized estimates')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_SpectraNormalized';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% assess reliability of original and spectrally normalized measure
   
h = figure('units','normalized','position',[.1 .1 .4 .3]);
subplot(1,2,1);
    a = nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,58:60,time>3 & time<6, 15),4),3),2);
    b = nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,1:4,58:60,time>3 & time<6, 15),4),3),2);
    scatter(a,b, 'filled'); lsline();
    [r, p] = corrcoef(a,b)
    title('BL: Spectrally normalized - original baselined SSVEP')
    xlabel('Relative SSVEP power orig. (a.u.)'); ylabel('Relative SSVEP power norm. (a.u.)')
    legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,2,2);
    a = nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,4,58:60,time>3 & time<6, 15)-OriginalSSVEP_bl(:,1,58:60,time>3 & time<6, 15),4),3),2);
    b = nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,4,58:60,time>3 & time<6, 15)-NormalizedSSVEP_bl(:,1,58:60,time>3 & time<6, 15),4),3),2);
    scatter(a,b, 'filled'); lsline();
    [r, p] = corrcoef(a,b)
    title('Spectrally normalized - original baselined SSVEP')
    xlabel('ChangeChange: Relative SSVEP power orig. (a.u.)'); ylabel('Relative SSVEP power norm. (a.u.)')
    legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_reliabilityNormalizedvsOriginalSSVEP';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot spectrally-normalized alpha and SSVEP

time = tmp.MTMdata{indCond}.time;

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,1); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 14),3)), [-2 2]); 
    title('Spectrally-norm. 28 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,2); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 15),3)), [-2 2]); 
    title('Spectrally-norm. SSVEP (30 Hz) power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,3); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 16),3)), [-2 2]); 
    title('Spectrally-norm. 32 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,4); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 14),3)), [-2 2]); 
    title('Orig. bl. 28 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,5); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 15),3)), [-2 2]); 
    title('Orig. bl. SSVEP (30 Hz) power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,6); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 16),3)), [-2 2]); 
    title('Orig. bl. 32 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_contrastNormalizedvsOriginalSSVEP';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% topoplot

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.FieldTrip    = [pn.root, 'B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.colormap = cBrew;

h = figure('units','normalized','position',[.1 .1 .2 .2]);
subplot(1,2,1);
    plotData = [];
    plotData.label = tmp.MTMdata{indCond}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-.1 .1];
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,:,time>3 & time<6, 15),4),2),1));
    ft_topoplotER(cfg,plotData);
    title({'30 Hz SSVEP';'without spectral norm.'})
subplot(1,2,2);
    %cfg.zlim = [-2.5*10^-10 2.5*10^-10];
    cfg.zlim = [-.3 .3];
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP(:,1:4,:,time>3 & time<6, 15),4),2),1));
    ft_topoplotER(cfg,plotData);
    title({'30 Hz SSVEP';'with spectral norm.'})
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_toposNormalizedvsOriginalSSVEP';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot mean SSVEP modulation by AMF

time = tmp.MTMdata{indCond}.time;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

idx_IDs = find(ismember(STSWD_summary.IDs, IDs));

%[sortVal, sortIdx] = sort(BS_meancent.data_linear, 'ascend');
%[sortVal, sortIdx] = sort(STSWD_summary.spectralslope_linearChange(idx_IDs,1), 'descend');
%[sortVal, sortIdx] = sort(STSWD_summary.MSE_slope(idx_IDs,1), 'ascend');
%[sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftEEG_linear(idx_IDs,1), 'ascend');
[sortVal, sortIdx] = sort(STSWD_summary.EEG_LV1.slope(idx_IDs,1), 'ascend');

% load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat');
% 
% idx_AttFactor = ismember(EEGAttentionFactor.IDs,IDs);
%[sortVal, sortIdx] = sort(EEGAttentionFactor.PCAalphaGamma(idx_AttFactor), 'ascend');

lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

% add within-subject error bars
pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
 
figure; 
hold on;
curData = squeeze(nanmean(nanmean(NormalizedSSVEP_bl(lowChIdx,1:4,58:60,:, 15),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'r','linewidth', 2}, 'patchSaturation', .1);
curData = squeeze(nanmean(nanmean(NormalizedSSVEP_bl(highChIdx,1:4,58:60,:, 15),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
title({'Low modulators are more entrained to the stimuli'})
legend([l1.mainLine, l2.mainLine],{'Low modulators'; 'High modulators'}, 'location', 'South'); legend('boxoff');
xlabel('Time (s)'); ylabel('SSVEP magnitude (normalized)');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% assemble: topoplot of SSVEP vs. baseline, load effect analysis

%% correlation of AMF with inter-individual differences in SSVEP

figure;
a = STSWD_summary.EEG_LV1.slope(idx_IDs,1);
b = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,1:4,58:60,time>5& time<6, 15),3),4),2));
scatter(a, b);
[r,p] = corrcoef(a, b)

%% correlation of drift

figure;
a = squeeze(nanmean(STSWD_summary.HDDM_vt.driftEEG(idx_IDs,1),2));
b = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,1,58:60,time>3& time<6, 15),3),4),2));
scatter(a, b);
[r,p] = corrcoef(a, b)

%% plot spectra depending on median-split SSVEP magnitude

x = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP(:,1:4,58:60,time>3& time<6, 15),3),4),2));

[sortVal, sortIdx] = sort(x, 'ascend');
lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

h = figure('units','normalized','position',[.1 .1 .4 .2]);
subplot(1,2,1); hold on;
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(lowChIdx,1,58:60,time>3 & time<6, :)),3),4));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 'r','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(highChIdx,1,58:60,time>3 & time<6, :)),3),4));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    title('Median split of normalized SSVEP, original spectra')
subplot(1,2,2); hold on;
    curData = squeeze(nanmean(nanmean(NormalizedSSVEP(lowChIdx,1,58:60,time>3 & time<6, :),3),4));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq(2:end)),nanmean(curData,1),standError, 'lineprops', {'color', 'r','linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(NormalizedSSVEP(highChIdx,1,58:60,time>3 & time<6, :),3),4));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq(2:end)),nanmean(curData,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
    title('Median split of normalized SSVEP, normalized spectra')
set(findall(gcf,'-property','FontSize'),'FontSize',15)

% This median-split analysis suggests a strong correlation between PSD
% slope and average SSVEP: more excitable state, lower SSVEP, larger alpha

%% joint plot of median-split normalized SSVEP by load

figure;
hold on; % plot low SSVEP
    condAvg = squeeze(nanmean(nanmean(nanmean(log10(OriginalSSVEP(lowChIdx,1:4,58:60,time>3 & time<6, :)),3),4),2));
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(lowChIdx,1,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(lowChIdx,2,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(lowChIdx,3,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(lowChIdx,4,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
hold on; % add % high changers
    condAvg = squeeze(nanmean(nanmean(nanmean(log10(OriginalSSVEP(highChIdx,1:4,58:60,time>3 & time<6, :)),3),4),2));
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(highChIdx,1,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(highChIdx,2,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(highChIdx,3,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(highChIdx,4,58:60,time>3 & time<6, :)),3),4));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);

    %% assess (anti-correlation) between average 1/f slopes and normalized SSVEP magnitude
    % may also be driven by outliers
    
    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_SlopeFits.mat'], 'SlopeFits')
    
    NormalizedSSVEP_bl_reg = NormalizedSSVEP_bl;
    NormalizedSSVEP_bl_reg(NormalizedSSVEP_bl_reg<0) = 0;
    
    SlopeFits_avg = squeeze(nanmean(nanmean(SlopeFits.linFit_2_30(:,1:4,58:60),3),2)); % average slopes across channels & conditions
	SSVEPmagnitude = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP_bl_reg(:,1:4,58:60,time>3& time<6, 15),3),4),2));
    Alphamagnitude = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP_bl_reg(:,1:4,58:60,time>3& time<6, 5),3),4),2));
    
    h = figure('units','normalized','position',[.1 .1 .3 .2]);
    subplot(1,2,1); hold on;
        curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(lowChIdx,4,58:60,time>3 & time<6, :)),3),4));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l1 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 'r','linewidth', 2}, 'patchSaturation', .1);
        curData = squeeze(nanmean(nanmean(log10(OriginalSSVEP(highChIdx,4,58:60,time>3 & time<6, :)),3),4));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        l2 = shadedErrorBar(log10(freq),nanmean(curData,1),standError, 'lineprops', {'color', 'k','linewidth', 2}, 'patchSaturation', .1);
        xlabel('Frequency [log10 Hz]'); ylabel('Power (a.u., log10)');
        xlim([min(log10(freq)) max(log10(freq))])
        title({'Median split of spectra';' by normalized SSVEP magnitude'})
    subplot(1,2,2); hold on;
        a = SlopeFits_avg;
        b = log10(SSVEPmagnitude);
        ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [.3 .5 .8], 'LineWidth', 3);
        scatter(a, b, 70, 'filled','MarkerFaceColor', [.3 .5 .8]);
        [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
        leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
           'location', 'SouthWest'); legend('boxoff')
        xlabel('Slope (log-log)'); ylabel('Normalized SSVEP (log10)');
        title({'PSD slope predicts normalized'; 'SSVEP magnitude'})
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/';
figureName = 'B_SpectralSlope_normalizedSSVEP';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
        

    SlopeFits_avg = squeeze(nanmean(nanmean(SlopeFits.linFit_2_30(:,1,58:60),3),2)); % average slopes across channels & conditions
	SSVEPmagnitude = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP(:,1,58:60,time>3& time<6, 15),3),4),2));
    Alphamagnitude = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP(:,1,58:60,time>3& time<6, 5),3),4),2));
    

    figure; hold on;
    a = SlopeFits_avg;
    b = Alphamagnitude;
    ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [.3 .5 .8], 'LineWidth', 3);
    scatter(a, b, 70, 'filled','MarkerFaceColor', [.3 .5 .8]);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    leg = legend([ls_1], {['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))]},...
       'location', 'SouthWest'); legend('boxoff')
    xlabel('Slope (log-log)'); ylabel('Normalized SSVEP (log10)');
    title('PSD slope predicts SSVEP magnitude')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
