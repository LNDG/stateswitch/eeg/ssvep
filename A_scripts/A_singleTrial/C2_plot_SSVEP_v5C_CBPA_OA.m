%% load all SSVEP v4 data

% v4 includes frequencies in 1 Hz steps, not just 30 +-2 Hz

%N = 53 OAs
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for indID = 1:numel(IDs)
    tmp = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/B_data/E_singleTrialFFT/',IDs{indID},'_SSVEP_FFT_singleTrial_byCond_v5.mat']);
    for indCond = 1:4
        OriginalSSVEP(indID,indCond,:,:,:) = tmp.MTMdata{indCond}.powspctrm_md;
    end
    time = tmp.MTMdata{indCond}.time;
    freq = tmp.MTMdata{indCond}.freq;
    elec = tmp.MTMdata{indCond}.elec;
end

% spectral correction

% NormalizedSSVEP = [];
% for indFreq = 3:30-2
%    NormalizedSSVEP(:,:,:,:,indFreq) = 10*log10(OriginalSSVEP(:,:,:,:,indFreq)./nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1, indFreq+1]),5));
% end

blTime = [2.3, 2.9];

for indFreq = 2:30-1
   NormalizedSSVEP(:,:,:,:,indFreq) = (OriginalSSVEP(:,:,:,:,indFreq)-nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5))./nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5);
end

blTimeIdx = find(tmp.MTMdata{indCond}.time > blTime(1) & tmp.MTMdata{indCond}.time <blTime(2));

originalBaseline = repmat(nanmean(nanmean(OriginalSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
normalizedBaseline = repmat(nanmean(nanmean(NormalizedSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);

OriginalSSVEP_bl = (OriginalSSVEP-originalBaseline)./originalBaseline;
NormalizedSSVEP_bl = (NormalizedSSVEP-normalizedBaseline);


figure; 
subplot(2,2,1); imagesc(squeeze(nanmean(NormalizedSSVEP_bl(:,4,44:60,:, 5),3)), [-1 1]); 
    title('Spectrally-normalized alpha power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,2,2); imagesc(squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 15),3)), [-1 1]); 
    title('Spectrally-normalized SSVEP power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,2,3); imagesc(squeeze(nanmean(NormalizedSSVEP_bl(:,4,:,:, 5),1)), [-1 1]); 
    title('Spectrally-normalized alpha power'); xlabel('Time (samples)'); ylabel('Channels');
subplot(2,2,4); imagesc(squeeze(nanmean(NormalizedSSVEP_bl(:,4,:,:, 15),1)), [-1 1]); 
    title('Spectrally-normalized SSVEP power'); xlabel('Time (samples)'); ylabel('Channels');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure; 
subplot(2,2,1); imagesc(squeeze(nanmean(OriginalSSVEP_bl(:,4,44:60,:, 5),3)),[-1 1]); 
    title('Spectrally-normalized alpha power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,2,2); imagesc(squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 15),3)),[-1 1]); 
    title('Spectrally-normalized SSVEP power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,2,3); imagesc(squeeze(nanmean(OriginalSSVEP_bl(:,4,:,:, 5),1)),[-1 1]); 
    title('Spectrally-normalized alpha power'); xlabel('Time (samples)'); ylabel('Channels');
subplot(2,2,4); imagesc(squeeze(nanmean(OriginalSSVEP_bl(:,4,:,:, 15),1)),[-1 1]); 
    title('Spectrally-normalized SSVEP power'); xlabel('Time (samples)'); ylabel('Channels');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

h = figure('units','normalized','position',[.1 .1 .6 .5]);
subplot(2,2,1); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('Original, non-normalized')
subplot(2,2,2); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('Original, temp. normalized')
subplot(2,2,3); imagesc(time,freq,squeeze(nanmean(nanmean(NormalizedSSVEP(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('spectrally normalized')
subplot(2,2,4); imagesc(time,freq,squeeze(nanmean(nanmean(NormalizedSSVEP_bl(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('spectrally, temp. normalized')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_TFR_OriginalNormalized_OA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


figure; 
subplot(2,2,1); imagesc(squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,4,44:60,:, :)-OriginalSSVEP_bl(:,1,44:60,:, :),3),1)))
subplot(2,2,2); imagesc(squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,4,58:60,:, :)-OriginalSSVEP_bl(:,1,58:60,:, :),3),1)))
subplot(2,2,3); imagesc(squeeze(nanmean(nanmean(NormalizedSSVEP(:,4,44:60,:, :)-NormalizedSSVEP(:,1,44:60,:, :),3),1)))
subplot(2,2,4); imagesc(squeeze(nanmean(nanmean(NormalizedSSVEP(:,4,58:60,:, :)-NormalizedSSVEP(:,1,58:60,:, :),3),1)))

% frontal delta-theta increase
figure; 
subplot(2,2,1); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,3,8:11,:, :)-OriginalSSVEP_bl(:,1,8:11,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)')
subplot(2,2,2); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,3,1:10,:, :)-OriginalSSVEP_bl(:,1,1:10,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)')


%% plot original and spectrally normalized spectra

h = figure('units','normalized','position',[.1 .1 .6 .5]);
subplot(2,1,1); hold on;
    plot(freq, squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,1,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(freq, squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,2,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(freq, squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,3,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(freq, squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,4,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    xlabel('Frequency (Hz)'); ylabel('Power (log10)');
    title('Non-normalized estimate');
    legend({'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}); legend('boxoff')
subplot(2,1,2); hold on;
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,1,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,2,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,3,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,4,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    xlabel('Frequency (Hz)'); ylabel('Power (norm.)');
    title('Spectrally-normalized estimates')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_SpectraNormalized_OA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% assess reliability of original and spectrally normalized measure
   
h = figure('units','normalized','position',[.1 .1 .6 .5]);
subplot(1,2,1);
    a = nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,58:60,time>3 & time<6, 15),4),3),2);
    b = nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,1:4,58:60,time>3 & time<6, 15),4),3),2);
    scatter(a,b, 'filled'); lsline();
    [r, p] = corrcoef(a,b)
    title('BL: Spectrally normalized - original baselined SSVEP')
    xlabel('Relative SSVEP power orig. (a.u.)'); ylabel('Relative SSVEP power norm. (a.u.)')
    legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,2,2);
    a = nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,4,58:60,time>3 & time<6, 15)-OriginalSSVEP_bl(:,1,58:60,time>3 & time<6, 15),4),3),2);
    b = nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,4,58:60,time>3 & time<6, 15)-NormalizedSSVEP_bl(:,1,58:60,time>3 & time<6, 15),4),3),2);
    scatter(a,b, 'filled'); lsline();
    [r, p] = corrcoef(a,b)
    title('Spectrally normalized - original baselined SSVEP')
    xlabel('ChangeChange: Relative SSVEP power orig. (a.u.)'); ylabel('Relative SSVEP power norm. (a.u.)')
    legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_reliabilityNormalizedvsOriginalSSVEP_OA';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% assess load effect of the two measures (t-test)

% figure;
%     a = max(nanmean(OriginalSSVEP_bl(1:47,:,55:60,time>3 & time<6, 15),4),[],3);
%     b = max(nanmean(NormalizedSSVEP_bl(1:47,:,55:60,time>3 & time<6, 15),4),[],3);
%     subplot(2,2,1); bar(nanmean(a,1))
%     subplot(2,2,2); bar(nanmean(b,1))
%     [~, p] = ttest(a(:,1), a(:,3))
%     [~, p] = ttest(b(:,1), b(:,2))
%     [~, p] = ttest(b(:,1), b(:,3))
%     [~, p] = ttest(b(:,1), b(:,4))
%     a = max(nanmean(OriginalSSVEP_bl(48:end,:,55:60,time>3 & time<6, 15),4),[],3);
%     b = max(nanmean(NormalizedSSVEP_bl(48:end,:,55:60,time>3 & time<6, 15),4),[],3);
%     subplot(2,2,3); bar(nanmean(a,1))
%     subplot(2,2,4); bar(nanmean(b,1))
%     [~, p] = ttest(a(:,1), a(:,3))
%     [~, p] = ttest(b(:,1), b(:,2))
%     [~, p] = ttest(b(:,1), b(:,3))
%     [~, p] = ttest(b(:,1), b(:,4))

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/T_tools'));
    
groups = {'YA'; 'OA'; 'all'};
conds={'Original', 'Spectr. norm.'};
idx_group{1} = 1:47;
idx_group{2} = 48:100;
idx_group{3} = 1:100;
condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel = [.2, .25, .3, .35, .4, .3];

h = figure('units','normalized','position',[.1 .1 .5 .7]);
cla;
for indGroup = 1:3
    for indCond = 1:2
        if indCond == 1
            curVar = max(nanmean(OriginalSSVEP_bl(idx_group{indGroup},:,51:60,time>3 & time<6, 15),4),[],3);
        elseif indCond == 2
            curVar = max(nanmean(NormalizedSSVEP_bl(idx_group{indGroup},:,51:60,time>3 & time<6, 15),4),[],3);  
        end
        subplot(3,2,(indGroup-1)*2+indCond);
        meanY = nanmean(curVar,1);
        % use within-subject error bars (Cousineau et al., 2005)
        % new value = old value ? subject average + grand average
        a_within = curVar-repmat(nanmean(curVar,2),1,size(curVar,2))+repmat(nanmean(nanmean(curVar,2),1),size(curVar,1),size(curVar,2));
        errorY = nanstd(a_within,[],1)/sqrt(size(a_within,1));
        [h1, hError] = barwitherr(errorY, meanY);
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(curVar(:,condPairs(indPair,1)), curVar(:,condPairs(indPair,2))) % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
                mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
            end
        end
        set(h1(1),'FaceColor','r');
        set(h1(1),'LineWidth',2);
        set(hError(1),'LineWidth',3);
        box(gca,'off')
        set(gca, 'XTick', [1,2,3,4]);
        set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
        xlabel('Target load')
        %xlabel('Target condition'); 
        ylabel({'SSVEP amp.'})
        title([groups{indGroup}, ': ', conds{indCond}]);
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        %ylim([0 .6]);
    end
end
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
% figureName = 'J_v5_loadEffect';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

%% theta-to-alpha switch

% figure;hold on;
% plot(freq, squeeze(nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,2,58:60,time>3 & time<6, :)-...
%     OriginalSSVEP(:,1,58:60,time>3 & time<6, :),3),4),1)))
% plot(freq, squeeze(nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,4,58:60,time>3 & time<6, :)-...
%     OriginalSSVEP(:,1,58:60,time>3 & time<6, :),3),4),1)))
% xlim([1 20])
% 
% figure;
% subplot(3,1,1); hold on;
% plot(freq(1:end-1), squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP_bl(:,2,58:60,time>3 & time<6, :)-...
%     NormalizedSSVEP_bl(:,1,58:60,time>3 & time<6, :),3),4),1)))
% plot(freq(1:end-1), squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP_bl(:,4,58:60,time>3 & time<6, :)-...
%     NormalizedSSVEP_bl(:,1,58:60,time>3 & time<6, :),3),4),1)))
% xlim([1 20])
% 
% subplot(3,1,2); hold on;
% plot(freq(1:end-1), squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP_bl(:,2,58:60,time>3 & time<6, :),3),4),1)))
% plot(freq(1:end-1), squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP_bl(:,4,58:60,time>3 & time<6, :),3),4),1)))
% xlim([1 20])
% 
% subplot(3,1,3); hold on;
% plot(freq, squeeze(nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,2,58:60,time>3 & time<6, :),3),4),1)-...
%     nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,1,58:60,time>3 & time<6, :),3),4),1)))
% plot(freq, squeeze(nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,4,58:60,time>3 & time<6, :),3),4),1)-...
%     nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,1,58:60,time>3 & time<6, :),3),4),1)))
% xlim([1 20])
% 
% x1 = squeeze(nanmedian(nanmedian(OriginalSSVEP(:,2,58:60,time>3 & time<6, :)-OriginalSSVEP(:,1,58:60,time>3 & time<6, :),3),4));
% %x1 = squeeze(nanmedian(nanmedian(OriginalSSVEP(:,2,58:60,time>3 & time<6, :)-OriginalSSVEP(:,1,58:60,time>3 & time<6, :),3),4));
% %igure; scatter(x1(:,5), x1(:,2))
% 
% figure;hold on;
% plot(freq, squeeze(nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,2,58:60,time>3 & time<6, :),3),4),1)))
% plot(freq, squeeze(nanmedian(nanmedian(nanmedian(OriginalSSVEP(:,4,58:60,time>3 & time<6, :),3),4),1)))
% xlim([1 20])

%% attempted anticorrelations between alpha and SSVEP

% % CPz:38;
% % Pz: 46;
% 
% a = nanmedian(nanmedian(nanmedian(OriginalSSVEP_bl(:,1:4,[38,46],:, 5),5),3),2);
% b = nanmedian(NormalizedSSVEP(:,1:4,58:60,:, 15),2);
% 
% figure; scatter(squeeze(nanmedian(nanmedian(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmedian(nanmedian(b(:,:,:,time>3 & time<6),4),3)), 'filled'); lsline();
% [r, p] = corrcoef(squeeze(nanmedian(nanmedian(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmedian(nanmedian(b(:,:,:,time>3 & time<6),4),3)))
% title('SSVEP-alpha anticorrelation')
% xlabel('Relative alpha power (a.u.)'); ylabel('Relative SSVEP power (a.u.)')
% legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% %%
% a = nanmean(nanmean(NormalizedSSVEP(:,1:4,46,:, 5),3),2);
% b = nanmean(NormalizedSSVEP_bl(:,1:4,58:60,:, 15),2);
% 
% figure; scatter(squeeze(nanmean(nanmean(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmean(nanmean(b(:,:,:,time>3 & time<6),4),3)), 'filled'); lsline();
% [r, p] = corrcoef(squeeze(nanmean(nanmean(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmean(nanmean(b(:,:,:,time>3 & time<6),4),3)))
% title('SSVEP-alpha anticorrelation')
% xlabel('Relative alpha power (a.u.)'); ylabel('Relative SSVEP power (a.u.)')
% legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% %%
% a = nanmean(nanmean(NormalizedSSVEP(:,1:4,55:60,:, 5),3),2);
% b = nanmean(NormalizedSSVEP(:,1:4,55:60,:, 15),2);
% 
% figure; scatter(squeeze(nanmean(nanmean(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmean(nanmean(b(:,:,:,time>3 & time<6),4),3)), 'filled'); lsline();
% [r, p] = corrcoef(squeeze(nanmean(nanmean(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmean(nanmean(b(:,:,:,time>3 & time<6),4),3)))
% title('SSVEP-alpha anticorrelation')
% xlabel('Relative alpha power (a.u.)'); ylabel('Relative SSVEP power (a.u.)')
% legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot spectrally-normalized alpha and SSVEP

time = tmp.MTMdata{indCond}.time;

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,1); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 14),3)), [-2 2]); 
    title('Spectrally-norm. 28 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,2); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 15),3)), [-2 2]); 
    title('Spectrally-norm. SSVEP (30 Hz) power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,3); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 16),3)), [-2 2]); 
    title('Spectrally-norm. 32 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,4); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 14),3)), [-2 2]); 
    title('Orig. bl. 28 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,5); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 15),3)), [-2 2]); 
    title('Orig. bl. SSVEP (30 Hz) power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,6); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 16),3)), [-2 2]); 
    title('Orig. bl. 32 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
figureName = 'J_v5_contrastNormalizedvsOriginalSSVEP';
% 
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');


%% topoplot

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.FieldTrip    = [pn.root, 'B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(1,2,1);
    plotData = [];
    plotData.label = tmp.MTMdata{indCond}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-.1 .1];
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,:,time>3 & time<6, 15),4),2),1));
    ft_topoplotER(cfg,plotData);
    title('30 Hz SSVEP without spectral normalization')
subplot(1,2,2);
    %cfg.zlim = [-2.5*10^-10 2.5*10^-10];
    cfg.zlim = [-.3 .3];
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP(:,1:4,:,time>3 & time<6, 15),4),2),1));
    ft_topoplotER(cfg,plotData);
    title('30 Hz SSVEP with spectral normalization')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/'; mkdir(pn.plotFolder);
% figureName = 'J_v5_toposNormalizedvsOriginalSSVEP';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

%% TMP: correlation between raw Pz alpha and SSVEP

% a = nanmean(nanmean(NormalizedSSVEP(:,1:4,44:50,:, 5),3),2);
% b = nanmean(NormalizedSSVEP_bl(:,1:4,55:60,:, 15),2);
% 
% figure; scatter(squeeze(nanmedian(nanmedian(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmedian(nanmedian(b(:,:,:,time>3 & time<6),4),3)), 'filled'); lsline();
% [r, p] = corrcoef(squeeze(nanmedian(nanmedian(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmedian(nanmedian(b(:,:,:,time>3 & time<6),4),3)))
% title('SSVEP-alpha anticorrelation')
% xlabel('Relative alpha power (a.u.)'); ylabel('Relative SSVEP power (a.u.)')
% legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% for indChan = 1:60
%     a = nanmean(nanmean(NormalizedSSVEP(:,1:4,indChan,:, 5),3),2);
%     b = nanmean(NormalizedSSVEP(:,1:4,58:60,:, 15),2);
%     %b = nanmean(NormalizedSSVEP(:,1:4,chanIdx,:, 15)-repmat(nanmean(NormalizedSSVEP(:,1:4,chanIdx,time>2 & time<3, 15),4),1,1,1,size(OriginalSSVEP,4),1),2);
%     [r, p] = corrcoef(squeeze(nanmean(nanmean(a(:,:,:,time>3 & time<6),4),3)),squeeze(nanmean(nanmean(b(:,:,:,time>3 & time<6),4),3)));
%     RMat(indChan) = r(2);
% end
% %figure; plot(RMat)
% 
% figure;
% cfg.zlim = [-.4 .4];
% plotData.powspctrm = RMat';
% ft_topoplotER(cfg,plotData);
% 
% chanIdx = find(ismember(tmp.MTMdata{indCond}.label, {'PO9', 'O1', 'Oz', 'O2', 'PO10'}));


%% CBPA: Normalized vs. baseline

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v2/';
addpath([pn.root, 'T_tools/fieldtrip-20161020/']); ft_defaults;


CBPAstruct = []; CBPAstructZero = [];
for indCond = 1:4
    CBPAstruct{indCond,1}.powspctrmdimord = 'rpt_chan_freq_time';
    CBPAstruct{indCond,1}.time = time;
    CBPAstruct{indCond,1}.freq = freq(1:end-1);
    CBPAstruct{indCond,1}.label = elec.label;
    CBPAstruct{indCond,1}.powspctrm = squeeze(permute(NormalizedSSVEP(:,indCond,:,:,:),[1,2,3,5,4]));
    CBPAstructZero{indCond,1} = CBPAstruct{indCond,1};
    CBPAstructZero{indCond,1}.powspctrm = squeeze(repmat(nanmean(...
        CBPAstructZero{indCond,1}.powspctrm(:,:,:,...
        CBPAstructZero{indCond,1}.time>blTime(1) & CBPAstructZero{indCond,1}.time<blTime(2)),4)...
        ,1,1,1,size(CBPAstructZero{indCond,1}.powspctrm,4)));
end

CBPAstructMerged.powspctrmdimord = 'rpt_chan_freq_time';
CBPAstructMerged.time = time;
CBPAstructMerged.freq = freq(1:end-1);
CBPAstructMerged.label = elec.label;
CBPAstructMerged.powspctrm = squeeze(nanmean(permute(NormalizedSSVEP(:,1:4,:,:,:),[1,2,3,5,4]),2));
CBPAstructMergedZero = CBPAstructMerged;
CBPAstructMergedZero.powspctrm = repmat(nanmean(...
    CBPAstructMergedZero.powspctrm(:,:,:,...
    CBPAstructMergedZero.time>blTime(1) & CBPAstructMergedZero.time<3),4)...
    ,1,1,1,numel(CBPAstructMergedZero.time));

%figure; imagesc(squeeze(nanmean(nanmean(CBPAstructMerged.powspctrm-CBPAstructMergedZero.powspctrm,2),1)))

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label;

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstructMergedZero);

subj = size(CBPAstructMergedZero.powspctrm,1);
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Norm_vsBL] = ft_freqstatistics(cfgStat, CBPAstructMergedZero, CBPAstructMerged);

%% CBPA: parametric load effect: Normalized

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1});

subj = size(CBPAstruct{1}.powspctrm,1);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Norm_param] = ft_freqstatistics(cfgStat, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});

%% original vs. baseline

CBPAstruct = []; CBPAstructZero = [];
for indCond = 1:4
    CBPAstruct{indCond,1}.powspctrmdimord = 'rpt_chan_freq_time';
    CBPAstruct{indCond,1}.time = time;
    CBPAstruct{indCond,1}.freq = freq;
    CBPAstruct{indCond,1}.label = elec.label;
    CBPAstruct{indCond,1}.powspctrm = squeeze(permute(OriginalSSVEP_bl(1:47,indCond,:,:,:),[1,2,3,5,4]));
    CBPAstructZero{indCond,1} = CBPAstruct{indCond,1};
    CBPAstructZero{indCond,1}.powspctrm = squeeze(repmat(nanmean(...
        CBPAstructZero{indCond,1}.powspctrm(:,:,:,...
        CBPAstructZero{indCond,1}.time>blTime(1) & CBPAstructZero{indCond,1}.time<blTime(2)),4)...
        ,1,1,1,size(CBPAstructZero{indCond,1}.powspctrm,4)));
end

CBPAstructMerged.powspctrmdimord = 'rpt_chan_freq_time';
CBPAstructMerged.time = time;
CBPAstructMerged.freq = freq;
CBPAstructMerged.label = elec.label;
CBPAstructMerged.powspctrm = squeeze(nanmean(permute(OriginalSSVEP_bl(1:47,1:4,:,:,:),[1,2,3,5,4]),2));
CBPAstructMergedZero = CBPAstructMerged;
CBPAstructMergedZero.powspctrm = repmat(nanmean(...
    CBPAstructMergedZero.powspctrm(:,:,:,...
    CBPAstructMergedZero.time>blTime(1) & CBPAstructMergedZero.time<3),4)...
    ,1,1,1,numel(CBPAstructMergedZero.time));

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstructMergedZero);

subj = size(CBPAstructMergedZero.powspctrm,1);
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Orig_vsBL] = ft_freqstatistics(cfgStat, CBPAstructMergedZero, CBPAstructMerged);

%% original parametric load effect

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1});

subj = size(CBPAstruct{1}.powspctrm,1);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Orig_param] = ft_freqstatistics(cfgStat, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});

%% save CBPA results

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/B_data/J_v5_CBPA_OA.mat',...
    'stat_Norm_vsBL', 'stat_Norm_param','stat_Orig_vsBL', 'stat_Orig_param')

%% plot CBPA effects

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,2,1)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Norm_vsBL.label(stat_Norm_vsBL.posclusterslabelmat>0);
    plotData = [];
    plotData.label = stat_Norm_vsBL.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Norm_vsBL.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    pval = []; pval = convertPtoExponential(stat_Norm_vsBL.posclusters(1).prob);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({'Spectrally normalized SSVEP (30 Hz)';['Stimulus vs. baseline: p = ', pval{1}]})
subplot(2,2,2)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Norm_param.label(stat_Norm_param.mask>0);
    plotData = [];
    plotData.label = stat_Norm_param.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Norm_param.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({'Spectrally normalized SSVEP (30 Hz)'})
subplot(2,2,3)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Orig_vsBL.label(nanmean(stat_Orig_vsBL.negclusterslabelmat(:,1),2)>0);
    plotData = [];
    plotData.label = stat_Orig_vsBL.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Orig_vsBL.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    pval = []; pval = convertPtoExponential(stat_Orig_vsBL.negclusters(1).prob);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({'Original SSVEP (30 Hz)';['Stimulus vs. baseline: p = ', pval{1}]})
subplot(2,2,4)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Orig_param.label(nanmean(stat_Orig_param.negclusterslabelmat(:,1),2)>0);
    plotData = [];
    plotData.label = stat_Orig_param.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Orig_param.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    pval = []; pval = convertPtoExponential(stat_Orig_param.negclusters(1).prob);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({'Original SSVEP (30 Hz)';['parametric effect: p = ', pval{1}]})

set(findall(gcf,'-property','FontSize'),'FontSize',30)
colormap(cBrew)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/C_figures/J/';
figureName = 'J_v5_CBPA_topographies';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
