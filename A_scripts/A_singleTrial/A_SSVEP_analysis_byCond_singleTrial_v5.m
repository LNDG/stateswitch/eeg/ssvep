%% analyse SSVEPs

% Here, we extract the 30 Hz SSVEP using intervals covering 15 cycles with
% a step size of 250 ms. Then we offer multiple outputs regarding
% normalization: spectral correction means that the average of the two
% adjacent frequency bands is subtracted from the 30 Hz estimate,
% effectively controlling for inter-individual differences in spectral
% slope and more broad-band noise around 30 Hz band.

% 190125 | JQK created script

% v5: CSD-transform, saves single-trial log10-transformed data

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
pn.FTout        = [pn.root, 'B_analyses/S2_TFR/B_data/B_TFout/']; mkdir(pn.FTout);
pn.FieldTrip    = [pn.root, 'B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47; N = 53 OAs
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)

    display(['processing ID ' IDs{id}]);

    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

     %% CSD transform
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    data.TrlInfo = TrlInfo; clear TrlInfo;
    data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;
    
    %% calculate FFT for stim period from ERP
    
    ncyc = 30;
    timeWindow = ncyc.*(1/30); % 1s sampling window
    stepSize = .1; % 100 ms step size
    timeWindows = 1:stepSize:8-(timeWindow./2);
    
    MTMdata = cell(1,4);

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

    for indCond = 1:4
        for indWindow = 1:numel(timeWindows)
            cfg = [];
            cfg.trials = find(data.TrlInfo(:,8)==indCond);
            cfg.toilim = [timeWindows(indWindow) timeWindows(indWindow)+timeWindow];
            curData = ft_redefinetrial(cfg, data);
            
            curData = rmfield(curData, 'TrlInfo');
            curData = rmfield(curData, 'TrlInfoLabels');

            cfg              = [];
            cfg.output       = 'pow';
            cfg.channel      = 'all';
            cfg.method       = 'mtmfft';
            cfg.taper        = 'hanning';
            cfg.keeptrials   = 'yes';
            %cfg.tapsmofrq    = 0;
            cfg.pad          = ceil(size(curData.time{1},2)/30)*30/100; % padding in s, such that cfg.foi/T is integer
            cfg.foi          = [2:2:60];
            %cfg.foi          = [ncyc-1:1:ncyc+1]/timeWindow;
            
            tmpData = ft_freqanalysis(cfg, curData);
            
            %figure; imagesc(squeeze(nanmean(tmpData.powspctrm,1)))
                        
            MTMdata{indCond}.powspctrm(:,:,indWindow,:) = tmpData.powspctrm;
            MTMdata{indCond}.dimord = 'rpt_chan_time_freq';
            MTMdata{indCond}.time(indWindow) = mean([timeWindows(indWindow) timeWindows(indWindow)+timeWindow]);
        end
        MTMdata{indCond}.label = tmpData.label;
        MTMdata{indCond}.freq = tmpData.freq;
        MTMdata{indCond}.elec = elec;
        MTMdata{indCond}.cfg = tmpData.cfg;
        clear tmpData;
    end

    % averaging and spectral correction using adjacent frequency bands
    
    for indCond = 1:4
        MTMdata{indCond}.powspctrm_avg = squeeze(nanmean(MTMdata{indCond}.powspctrm,1));
        MTMdata{indCond}.powspctrm_avg_log10 = squeeze(nanmean(log10(MTMdata{indCond}.powspctrm),1));
        MTMdata{indCond}.powspctrm_avg_SpectNorm13 = squeeze(nanmean(MTMdata{indCond}.powspctrm(:,:,:,15)-nanmean(MTMdata{indCond}.powspctrm(:,:,:,[14 16]),4),1));
        
        MTMdata{indCond}.powspctrm_md = squeeze(nanmedian(MTMdata{indCond}.powspctrm,1));
        MTMdata{indCond}.powspctrm_md_log10 = squeeze(nanmedian(log10(MTMdata{indCond}.powspctrm),1));
        MTMdata{indCond}.powspctrm_md_SpectNorm13 = squeeze(nanmedian(MTMdata{indCond}.powspctrm(:,:,:,15)-nanmedian(MTMdata{indCond}.powspctrm(:,:,:,[1,3]),4),1));
        
        MTMdata{indCond}.dimord = 'chan_time_freq';
        MTMdata{indCond} = rmfield(MTMdata{indCond}, 'powspctrm');
    end
       
    %% save output
    
    pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/B_data/E_singleTrialFFT/';
    save([pn.dataOut, IDs{id}, '_SSVEP_FFT_singleTrial_byCond_v5.mat'], 'MTMdata');
        
end % ID
